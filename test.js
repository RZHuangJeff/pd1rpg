let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources
    Sprite = PIXI.Sprite;

var testScene = '{ \
    "background": "images/background.png", \
    "origin": {"x": 423, "y": 405}, \
    "terrain": { \
        "boarder": {"x": [0, 1080], "y": [0, 720]}, \
        "reachable": [ \
            {"x0": 423, "y0": 258, "x1": 423, "y1": 702} \
        ] \
    }, \
    "objects": { \
        "npc1": { \
            "type": "normal", "construct": true, "position": {"x": 423, "y": 225}, "visible": true, \
            "events": [ \
                { \
                    "conditions": {"range": 36, "key": "t"}, \
                    "actions": [ \
                        {"type": "character", "position": {"x": 423, "y": 700}}, \
                        {"type": "object", "tag": "npc2", "visible": true}, \
                        {"type": "object", "tag": "npc3", "construct": true} \
                    ] \
                } \
            ] \
        }, \
        "npc2": {"type": "normal", "construct": true, "position": {"x": 387, "y": 379}, "visible": false, \
            "events": [ \
                { \
                    "conditions": {"range": 72, "key": "t"}, \
                    "actions": [ \
                        {"type": "dialog", "tag": "dialog1"} \
                    ] \
                } \
            ] \
        }, \
        "npc3": {"type": "normal", "construct": false, "position": {"x": 459, "y": 600}, "visible": true, "events": []}, \
        "dialog1": {"type": "dialog", "construct": false, "talks": [ \
            {"type": "normal", "speaker": "a", "next": 1, "text": "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"}, \
            {"type": "choose", "speaker": "b", "options": [{"text": "option1", "next": -1}, {"text": "option2", "next": 2}]}, \
            {"type": "url", "url": "sample.html", "next": 1} \
        ]} \
    } \
}'

class Keyboard {
    static init() {
        Keyboard.key = null;
        Keyboard.got = false;

        window.addEventListener("keydown", Keyboard.keydown, false);
        window.addEventListener("keyup", Keyboard.keyup, false);
    }

    static isKeyPressed(key) {
        return key === Keyboard.key;
    }

    static pressingKey() {
        return Keyboard.key;
    }

    static keydown(event){
        Keyboard.key = event.key;
    }

    static keyup(event){
        Keyboard.got = false;
        Keyboard.key = null;
    }
}

class Character {
    constructor(){
        this.char = new PIXI.Container();
        
        var graph = new PIXI.Graphics();
        graph.beginFill(0xFF0000);
        graph.drawRect(0, 0, 36, 36);
    
        this.char.addChild(graph);
        this.char.pivot.set(18, 18);

        this.speed = 0;
        this.dir = 'none';
    }

    setPosition(newPos) {
        this.char.position.set(newPos.x, newPos.y);
    }

    getPosition(){
        return {x: this.char.position.x, y: this.char.position.y};
    }

    getNextPosition() {
        var curPos = {x: this.char.position.x, y: this.char.position.y};

        switch (this.dir) {
            case 'up':
                curPos.y -= 36*this.speed;
                break;
            case 'down':
                curPos.y += 36*this.speed;
                break;
            case 'left':
                curPos.x -= 36*this.speed;
                break;
            case 'right':
                curPos.x += 36*this.speed;
                break;
            default:
                break;
        }

        return curPos;
    }

    setDirection(newDir) {
        this.dir = newDir;
    }

    setSpeed(newSpeed) {
        this.speed = newSpeed;
    }

    move() {
        var newPos = this.getNextPosition();

        this.char.position.set(newPos.x, newPos.y);
    }
}

class GameObject {
    constructor (obj) {
        this.container = new PIXI.Container();
    }

    pollingEvents(charPos, key) {
        return [];
    }

    update(delta) {}
}

class NormalObject extends GameObject {
    constructor (obj) {
        super(obj);

        this.container.pivot.set(18, 18);
        this.container.position.set(obj.position.x, obj.position.y);
        this.container.visible = obj.visible;

        if(obj.src){

        }
        else{
            var g = new PIXI.Graphics();
            g.beginFill(0x0000FF);
            g.drawRect(0, 0, 36, 36);
            g.endFill();

            this.container.addChild(g);
        }

        this.events = obj.events;
    }

    pollingEvents(charPos, key) {
        var actions = [];

        this.events.forEach(e => {
            var obj = this.container;
            var cond = e.conditions;

            if(distance(obj.position, charPos) <= cond.range && key === cond.key)
                e.actions.forEach(act => {
                    actions.push(act);
                });
        });

        return actions;
    }
}

class DialogObject extends GameObject {
    constructor (obj) {
        super(obj);

        var g = new PIXI.Graphics();
        g.beginFill(0xF5DEB3);
        g.drawRoundedRect(5, 5, 1070, 350, 10);
        g.endFill();

        g.beginFill(0xFEFEFE);
        g.drawRoundedRect(10, 10, 1060, 340, 10);
        g.endFill();

        g.beginFill(0xDCDCDC);
        g.drawRoundedRect(25, 0, 100, 40, 5);
        g.endFill();

        this.container.addChild(g);

        this.dot = new PIXI.Graphics();
        this.dot.beginFill(0xFF0000);
        this.dot.drawCircle(0, 0, 5);
        this.dot.position.set(40, 68);
        this.dot.endFill();
        this.dot.visible = false;

        this.container.addChild(this.dot);

        this.speaker = new PIXI.Text("", {fill: 0x333333});
        this.speaker.position.set(75, 5);

        this.content = new PIXI.Text("", {fill: 0x333333});
        this.content.position.set(50, 50);

        this.container.addChild(this.speaker);
        this.container.addChild(this.content);

        this.container.position.set(0, 360);

        this.talks = obj.talks;
        this.jumped = false;
        this.parag = 0;
        this.index = 0;
    }
    
    pollingEvents(charPos, key) {
        var curParag = this.talks[this.parag];

        if(key == 'Enter'){
            if(!this.jumped)
                if(curParag.type == 'normal'){
                    if(this.index < curParag.text.length)
                        this.index = curParag.text.length - 1;
                    else {
                        this.parag = curParag.next;
                        this.index = 0;
                    }
                }
                else {
                    this.parag = curParag.options[this.index].next;
                    this.index = 0;
                }
            this.jumped = true;
        }
        else if(key == 'w'){
            if(!this.jumped && curParag.type == 'choose')
                if(this.index > 0)
                    this.index--;
            this.jumped = true;
        }
        else if(key == 's'){
            if(!this.jumped && curParag.type == 'choose')
                if(this.index < curParag.options.length - 1)
                    this.index++;
            this.jumped = true;
        }
        else
            this.jumped = false;

        if(this.parag == -1)
            return [{type: 'kill', tag: 'dialog1'}];
        if(this.talks[this.parag].type == 'url')
            return [{type: 'url', url: this.talks[this.parag].url}];
        return [];
    }

    update(delta) {
        var curParag = this.talks[this.parag];
        this.speaker.text = curParag.speaker;

        if(curParag.type == 'normal'){
            this.dot.visible = false;
            if(this.index < curParag.text.length)
                this.content.text = curParag.text.slice(0, this.index++);
        }
        else if(curParag.type == 'choose'){
            this.dot.visible = true;
            this.dot.position.y = 68 + this.index*26;
            this.content.text = "";
            curParag.options.forEach(opt => {
                this.content.text += opt.text + '\n';
            });
        }
        else if(curParag.type == 'url'){
            this.parag = curParag.next;
        }
    }
}

class ObjectBuilder {
    static build(obj) {
        switch (obj.type) {
            case 'normal':
                return new NormalObject(obj);
            case 'dialog':
                return new DialogObject(obj);
            default:
                break;
        }
    }
}

class Scene {
    constructor(sceneJson) {
        this.sc = JSON.parse(sceneJson);
    
        this.background = new Sprite(PIXI.Texture.from(this.sc.background))
        this.origin = this.sc.origin;
        this.terrain = this.sc.terrain;

        this.focus = null;

        this.initObjects();
    }

    initObjects() {
        this.objects = {};
        for(var key in this.sc.objects){
            if(this.sc.objects[key].construct)
                this.objects[key] = ObjectBuilder.build(this.sc.objects[key]);
        }
    }

    isReachable(pos) {
        if(pos.x <= this.terrain.boarder.x[0] || pos.x >= this.terrain.boarder.x[1])
            return false;

        if(pos.y <= this.terrain.boarder.y[0] || pos.y >= this.terrain.boarder.y[1])
            return false;

        var result = false;
        this.terrain.reachable.forEach(res => {
            if(pos.x >= res.x0 && pos.x <= res.x1 &&
                pos.y >= res.y0 && pos.y <= res.y1)
                return result = true;
        });

        return result;
    }

    handleInput(game, character) {
        var charPos = character.getPosition();
        var key = Keyboard.pressingKey();

        if(this.focus){
            var actions = this.focus.pollingEvents(charPos, key);
            this.doActions(actions, game, character);
        }
        else{
            for(var k in this.objects){
                var actions = this.objects[k].pollingEvents(charPos, key);
                this.doActions(actions, game, character);
            }
        }
    }

    update(delta) {
        if(this.focus)
            this.focus.update(delta);
    }

    doActions(actions, game, character) {
        actions.forEach(acts => {
            switch (acts.type) {
                case 'character':
                    if(acts.position != null)
                        character.setPosition(acts.position);
                    break;
                case 'object':
                    if(acts.construct != null)
                        this.objects[acts.tag] = ObjectBuilder.build(this.sc.objects[acts.tag]);
                        game.addObject(this.objects[acts.tag]);

                    var target = this.objects[acts.tag];
                    
                    if(acts.visible != null)
                        target.container.visible = acts.visible;
                    
                    break;
                case 'dialog':
                    this.objects[acts.tag] = ObjectBuilder.build(this.sc.objects[acts.tag]);
                    this.focus = this.objects[acts.tag];
                    game.addObject(this.focus);
                    break;
                case 'kill':
                    var toKill = this.objects[acts.tag];
                    if(toKill == this.focus)
                        this.focus = false;

                    game.removeObject(toKill);
                    delete this.objects[acts.tag];
                    break;
                case 'url':
                    console.log(acts);
                    window.open(acts.url, "_blank");
                    break;
                default:
                    break;
            }
        });
    }
}

class Game {
    constructor(scene) {
        this.initApp();

        this.scene = scene;
        this.loadScene();

        this.character = new Character();
        this.character.setPosition(this.scene.origin);
        this.stage.addChild(this.character.char);
    }

    initApp(){
        this.app = new Application({width: 1080, height: 720});
        this.stage = this.app.stage;
        document.body.appendChild(this.app.view);

        this.app.renderer.view.style.position = "absolute";
        this.app.renderer.view.style.display = "block";
        this.app.renderer.view.style.right = (window.innerWidth - 1080)/2 + "px";
        this.app.renderer.view.style.top = (window.innerHeight - 720)/2 + "px";
    }

    loadScene() {
        this.stage.addChild(this.scene.background);
        for(var key in this.scene.objects)
            this.stage.addChild(this.scene.objects[key].container);
    }

    addObject(obj) {
        this.stage.addChild(obj.container);
    }

    removeObject(obj) {
        this.stage.removeChild(obj.container);
    }

    start(){
        this.app.ticker.add(delta => this.gameLoop(delta))
    }

    gameLoop(delta){
        this.handleInput();
        this.update(delta);
    }

    handleInput() {
        this.character.setDirection('none');

        if(Keyboard.isKeyPressed('w'))
            this.character.setDirection('up');

        if(Keyboard.isKeyPressed('s'))
            this.character.setDirection('down');
        
        if(Keyboard.isKeyPressed('a'))
            this.character.setDirection('left');

        if(Keyboard.isKeyPressed('d'))
            this.character.setDirection('right');

        this.scene.handleInput(this, this.character);
    }

    update(delta) {
        this.character.setSpeed(delta/5);
        if(this.scene.isReachable(this.character.getNextPosition()))
            this.character.move();

        this.scene.update(delta);
    }
}

Keyboard.init();

var scene = new Scene(testScene);

var game = new Game(scene);
game.start();


function distance(p1, p2) {
    var dx = p1.x - p2.x;
    var dy = p1.y - p2.y;

    return Math.floor(Math.sqrt(dx*dx + dy*dy));
}